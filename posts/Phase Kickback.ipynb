{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Phase Kickback\n",
    "\n",
    "Phase kickback is an important concept in quantum computing. It is used in a variety of algorithms such as phase estimation, Grover's Algorithm, Deutsch's Algorithm, and more.\n",
    "\n",
    "The core operation of phase kickback is to take one of the states in a linear superposition and apply some phase factor to that state. In other words, given the state vector $\\mid\\phi\\rangle$, our goal is to rotate one of the states $\\mid\\phi_i\\rangle$ by a phase factor $e^{-i\\theta}$ while leaving the rest unaffected. We will consider the operator $U_i$ to be the unitary operator which applies our phase transform $e^{-i\\theta}$ to the $\\mid\\phi_i\\rangle$ in the given system.\n",
    "\n",
    "Using the bra-ket notation, we can describe this operation as such:\n",
    "\n",
    "$$U_i\\mid\\phi\\rangle = \\mid\\phi_{0..00}\\rangle + \\mid\\phi_{0..01}\\rangle + ... + e^{i\\theta}\\mid\\phi_{i}\\rangle + ... + \\mid\\phi_{1..11}\\rangle $$\n",
    "\n",
    "There are a few methodologies by which this can be done. Some require ancillary qubits while other variants do not. We will describe the transform initially without an ancilla qubit, then later introduce the ancillary version of the same operation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Phase Kickback Operation - Example\n",
    "\n",
    "Lets say we have $2$ qubits in a linear, uniform superposition of $\\mid\\phi\\rangle = \\frac{1}{2}(\\mid 00\\rangle + \\mid 01\\rangle + \\mid 10\\rangle + \\mid 11\\rangle)$. From this, we have four unique states:\n",
    "\n",
    "$$\\mid\\phi_{00}\\rangle = \\frac{1}{2}\\mid 00\\rangle$$\n",
    "$$\\mid\\phi_{01}\\rangle = \\frac{1}{2}\\mid 01\\rangle$$\n",
    "$$\\mid\\phi_{10}\\rangle = \\frac{1}{2}\\mid 10\\rangle$$\n",
    "$$\\mid\\phi_{11}\\rangle = \\frac{1}{2}\\mid 11\\rangle$$\n",
    "\n",
    "For now, we'll simplify our phase kickback operation by applying a phase of $e^{i\\pi}$. This means that for our state $\\mid\\phi_i\\rangle$, we will apply the operation $e^{i\\pi}\\mid\\phi_i\\rangle = -1\\mid\\phi_i\\rangle$ while the rest of our states are unchanged.\n",
    "\n",
    "Lets say we want to apply the phase operation to state $\\mid 10\\rangle$. We need a operation $U_{10}$ operating on both qubits to produce the following state:\n",
    "\n",
    "$$U_{10}\\mid\\phi\\rangle = \\frac{1}{2}(\\mid 00\\rangle + \\mid 01\\rangle - \\mid 10\\rangle + \\mid 11\\rangle)$$\n",
    "\n",
    "Our operation $U_{10}$ took the state vector coefficients from $\\frac{1}{2}(1, 1, 1, 1)$ to state $\\frac{1}{2}(1, 1, -1, 1)$. If we write operation $U_{10}$ as a matrix operating on the state $\\mid\\phi\\rangle$, we have the following matrix:\n",
    "\n",
    "$$U_{10}=\\begin{bmatrix} 1 & 0 & 0 & 0 \\\\ 0 & 1 & 0 & 0 \\\\ 0 & 0 & -1 & 0 \\\\ 0 & 0 & 0 & 1\\end{bmatrix}$$\n",
    "\n",
    "We can verify this by applying the operation to our original state vector $\\mid\\phi\\rangle$:\n",
    "\n",
    "$$U_{10}\\mid\\phi\\rangle = \n",
    "\\begin{bmatrix} 1 & 0 & 0 & 0 \\\\ 0 & 1 & 0 & 0 \\\\ 0 & 0 & -1 & 0 \\\\ 0 & 0 & 0 & 1\\end{bmatrix}\n",
    "\\begin{bmatrix}\\frac{1}{2} \\\\ \\frac{1}{2} \\\\ \\frac{1}{2} \\\\ \\frac{1}{2}\\end{bmatrix}\n",
    "=\n",
    "\\begin{bmatrix}\\frac{1}{2} \\\\ \\frac{1}{2} \\\\ -\\frac{1}{2} \\\\ \\frac{1}{2}\\end{bmatrix}$$\n",
    "\n",
    "From here, it is evident how we could construct a gate to change the phase for any of the four states. For example:\n",
    "\n",
    "$$U_{00} = \\begin{bmatrix} -1 & 0 & 0 & 0 \\\\ 0 & 1 & 0 & 0 \\\\ 0 & 0 & 1 & 0 \\\\ 0 & 0 & 0 & 1\\end{bmatrix}$$\n",
    "\n",
    "For the example, we substituted in $e^{i\\pi}$ substituting $\\pi$ for $\\theta$ as our phase value. However, to make the operation more generic, we must substitute back in our general phase operator. To do this, we simply substitute $-1$ in our gate operations with $e^{i\\theta}$. e.g:\n",
    "\n",
    "$$U_{10}\\mid\\phi\\rangle = \\begin{bmatrix} 1 & 0 & 0 & 0 \\\\ 0 & 1 & 0 & 0 \\\\ 0 & 0 & e^{i\\theta} & 0 \\\\ 0 & 0 & 0 & 1\\end{bmatrix}$$\n",
    "\n",
    "This has the effect of taking state $\\mid\\phi_{i}\\rangle$ to $e^{i\\theta}\\mid\\phi_{i}\\rangle$ and leaves the rest of the states unaffected.\n",
    "\n",
    "We can scale this to $N$ qubits by creating a $2^Nx2^N$ operator which only changes the phase of one state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Circuit Construction\n",
    "\n",
    "Ideally, we would construct a quantum computer which would allow us to rotate any state in our state vector by the given phase in a single timestep. In other words, $U\\mid\\phi\\rangle$ would take only one timestep. In reality, that's not the case. We often are restricted to a set of gates by which we can apply our operations to qubits either due to physical constraints or limitations of the computer. This limitation of computing inspires the different methods of circuit construction shown here.\n",
    "\n",
    "There are two methods to construct the gate $U$ for any arbitrary state in our state vector $\\mid\\phi\\rangle$. The first method uses no ancillary qubits while the second method does rely on a single ancillary qubit. Depending on your system or algorithm, there are situations where one may be preferable to the other.\n",
    "\n",
    "We will use $H$ or Hadamard gates to create the uniform superposition. To get us into our superposition, we apply the $H$ gate to all qubits. Mathematically speaking: \n",
    "- Let our starting state be $\\mid 00\\rangle$\n",
    "- $H^{\\otimes 2}\\mid 00\\rangle= \\frac{1}{2}(\\mid 00\\rangle + \\mid 01\\rangle + \\mid 10\\rangle + \\mid 11\\rangle)$\n",
    "\n",
    "This is applicable to all described methods. We will assume that we start in this state so that $\\mid\\phi\\rangle=\\frac{1}{2}(\\mid 00\\rangle + \\mid 01\\rangle + \\mid 10\\rangle + \\mid 11\\rangle)$. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### No Ancillary Method\n",
    "\n",
    "The first method can be done using the following gates:\n",
    "\n",
    "- $C_{\\theta}$ (Controlled phase gate)\n",
    "- $X$ (The \"NOT\" gate)\n",
    "\n",
    "We require a controlled phase gate which will apply a phase to only one state in our vector. For simplicity, we will again consider the phase $e^{i\\pi}$. We will also consider a gate which only applies the transform $U_{11}$. The gate which commonly performs this operation is refered to as the $C_z$ or Controlled-Z gate. This gate can be written as:\n",
    "\n",
    "$$\\begin{bmatrix}1 & 0 & 0 & 0 \\\\ 0 & 1 & 0 & 0 \\\\ 0 & 0 & 1 & 0 \\\\ 0 & 0 & 0 & -1\\end{bmatrix}$$\n",
    "\n",
    "From here, it is evident that applying the operation to our superposition will transform the state vector $\\mid\\phi\\rangle$ to $\\frac{1}{2}(\\mid 00\\rangle + \\mid 01\\rangle + \\mid 10\\rangle - \\mid 11\\rangle)$.\n",
    "\n",
    "#### The \"Kickback\" in Phase Kickback\n",
    "\n",
    "Now we have a gate which can apply the transform to a single state $\\mid11...1\\rangle$ in our state vector. However, for a proper phase kickback, we need to be able to \"kick back\" the applied phase value to any other state. That's where our $X$ gates come in.\n",
    "\n",
    "The $X$ gate is commonly used to shuffle coefficients around in our state vector. For a simple example, we start with a qubit in the following state:\n",
    "\n",
    "$$\\mid\\psi\\rangle=\\alpha\\mid0\\rangle - \\beta\\mid1\\rangle$$\n",
    "\n",
    "We apply the operation $X$ to our state $\\mid\\psi\\rangle$ to get the following:\n",
    "\n",
    "$$\\frac{1}{\\sqrt{2}}\\begin{bmatrix} \\alpha \\\\ -\\beta \\end{bmatrix}\n",
    "\\begin{bmatrix}0 & 1 \\\\ 1 & 0\\end{bmatrix} =\n",
    "\\frac{1}{\\sqrt{2}}\\begin{bmatrix} -\\alpha \\\\ \\beta \\end{bmatrix}$$\n",
    "\n",
    "Depending on the combination of X gates we apply to our qubits, we can kick back our phase rotation to any other state. In our 2 qubit case, applying the X gate to both qubits will shuffle the phase factor back to $\\mid 00\\rangle$ so that $\\mid\\phi\\rangle$ would be transformed to $\\frac{1}{2}(-\\mid 00\\rangle + \\mid 01\\rangle + \\mid 10\\rangle + \\mid 11\\rangle)$.\n",
    "\n",
    "We can apply the same substitutions as above to replace the $C_{z}$ gate with an arbitrary Controlled-$\\theta$ gate for any arbitrary phase rotation. In this case, we would need a programmable gate to fill in our value for $\\theta$, but that may not always be available. However, many other phase rotations exist such as the very common $T$ gate which is a $\\pi/8$ phase rotation or $S$ gate which is a phase rotation $\\pi/4$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
